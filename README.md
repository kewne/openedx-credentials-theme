# openedx-credentials-theme

Test project for creating a theme for the Open edX Credentials IDA.

## Installing 

These instructions are based off the [Theming documentation for the Credentials IDA](https://edx-credentials.readthedocs.io/en/latest/theming.html).

Install this package:
```sh
# directly from version control
pip install git+https://gitlab.com/kewne/openedx-credentials-theme.git#egg=openedx-credentials-theme-simple

# from a local checkout (use -e flag if testing local changes)
pip install PATH_TO_REPO
```

Then, add the app to `INSTALLED_APPS` in the Django settings:
```python
INSTALLED_APPS += ['openedx_credentials_theme_simple']
```

Go to `CREDENTIALS_IDA_BASE/admin/core/siteconfiguration/` and set **Theme Name** to `simple`.

Finally go to `CREDENTIALS_IDA_BASE/credentials/example/` and verify that your styles apply.