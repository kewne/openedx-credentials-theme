from django.apps import AppConfig


class OpenedxCredentialsThemeSimpleConfig(AppConfig):
    name = 'openedx_credentials_theme_simple'
